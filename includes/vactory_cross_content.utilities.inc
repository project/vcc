<?php

/**
 * @file
 * Utilities functions of vactory_cross_content module.
 */

/**
 * Get content type fields as array options.
 */
function _vactory_cross_content_get_term_reference_fields($type) {
  $fields = field_info_instances("node", $type->type);
  $options_terms = array();
  foreach ($fields as $key => $value) {
    $field_info = field_info_field($key);
    if (in_array($field_info['module'], array('taxonomy'))) {
      $options_terms[$key] = $value['label'];
    }
  }
  return $options_terms;
}

/**
 * Get content type names as array options.
 */
function _vactory_cross_content_get_ct_names() {
  $ct = node_type_get_types();
  $types = array();
  foreach ($ct as $mn => $content) {
    $types[$mn] = $content->name;
  }
  return $types;
}

/**
 * Set cross_content_field for enabled types.
 */
function _vactory_cross_content_set_cc_field($type, $ct_to_include, $enabled) {

  $field_name = substr($type->type,0,23);
  $field_name = "field_{$field_name}_cc";

  if ($enabled) {

    $target_bundles = array();
    foreach ($ct_to_include as $ct) {
      if ($ct !== 0) {
        $target_bundles[$ct] = $ct;
      }
    }

    // Create cross content field.
    if (!field_info_field($field_name)) {
      // Create the field base.
      $field = array(
        'field_name' => $field_name,
        'type' => 'entityreference',
        'cardinality' => FIELD_CARDINALITY_UNLIMITED,
        'settings' => array(
          'target_type' => 'node',
          'handler_settings' => array('target_bundles' => $target_bundles),
        ),
      );
      if (module_exists("translated_entityreference")) {
        $field['settings']['handler'] = 'translated_entityreference';
        $field['settings']['handler_settings']['filter']['language'] = 1;
      }
      field_create_field($field);
    }
    else {
      $fields = field_info_fields();
      $cc_field = $fields[$field_name];
      $cc_field['settings']['handler_settings']['target_bundles'] = $target_bundles;
      field_update_field($cc_field);
    }

    // Ajouter l'instance du champ cross content aux types de contenu activé.
    if (!field_info_instance('node', $field_name, $type->type)) {
      // Create the field instance on the bundles.
      $instance = array(
        'field_name' => $field_name,
        'entity_type' => 'node',
        'bundle' => $type->type,
        'label' => t('Contenus liés'),
        'widget' => array(
          'type' => 'options_select',
        ),
      );
      field_create_instance($instance);
    }
  }
  else {
    field_delete_field($field_name);
  }
}
